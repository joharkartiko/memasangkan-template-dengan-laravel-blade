@extends('layouts.master')

@section('title')
    List Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$item)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$item->nama}}</td>
                        <td>{{$item->umur}}</td>
                        <td>{{$item->bio}}</td>
                        <td>
                           
                            <form action="/cast/{{$item->id}}" method="POST">
                                @csrf
                                @method('delete')
                                <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                                <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                                <input Type="submit" class="btn btn-danger btn-sm" value="delete">
                            </form>
                    </tr>
                @empty
                     <tr>
                         <td>TIdak ada Data</td>
                     </tr>
                @endforelse              
            </tbody>
        </table>
@endsection